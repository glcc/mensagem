//
//  ResultViewController.swift
//  Mensagens
//
//  Created by Gerson  on 12/08/2018.
//  Copyright © 2018 Gerson . All rights reserved.
//

import UIKit

class ResultViewController: BaseViewController {
    
    var useWhiteBorder: Bool = false
    
    @IBOutlet weak var viBorder: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.setNavigationBarHidden(true, animated: true)
        
        lbMessage.text = message.text
        lbMessage.textColor = message.textColor
        lbMessage.backgroundColor = message.backgroundColor
        view.backgroundColor = message.screenColor
        viBorder.backgroundColor = useWhiteBorder ? .white : .clear
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        navigationController?.popViewController(animated: true)
    }

}

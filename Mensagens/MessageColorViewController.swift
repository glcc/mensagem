//
//  MessageColorViewController.swift
//  Mensagens
//
//  Created by Gerson  on 12/08/2018.
//  Copyright © 2018 Gerson . All rights reserved.
//

import UIKit

class MessageColorViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        lbMessage.text = message.text
        lbMessage.textColor = message.textColor
        lbMessage.backgroundColor = message.backgroundColor
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as! ScreenColorViewController
        vc.message = message
    }

}

extension MessageColorViewController: ColorPickerDelegate {
    func applyColor(color: UIColor) {
        lbMessage.backgroundColor = color
        message.backgroundColor = color
    }
}

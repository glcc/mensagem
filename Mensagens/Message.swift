//
//  Mensagem.swift
//  Mensagens
//
//  Created by Gerson  on 12/08/2018.
//  Copyright © 2018 Gerson . All rights reserved.
//

import UIKit

struct Message {
    var text: String?
    var textColor: UIColor?
    var backgroundColor: UIColor?
    var screenColor: UIColor?
}
